import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'liste-panneau', loadChildren: './tab-consulter/liste-panneau/liste-panneau.module#ListePanneauPageModule' },
  { path: 'detail-panneau', loadChildren: './tab-consulter/detail-panneau/detail-panneau.module#DetailPanneauPageModule' },
  { path: 'menu-panneau', loadChildren: './tab-consulter/menu-panneau/menu-panneau.module#MenuPanneauPageModule' },
  { path: 'question', loadChildren: './tab-quizz/question/question.module#QuestionPageModule' },
  { path: 'detail-categorie', loadChildren: './tab-consulter/detail-categorie/detail-categorie.module#DetailCategoriePageModule' },
  { path: 'menu-categorie', loadChildren: './tab-consulter/menu-categorie/menu-categorie.module#MenuCategoriePageModule' },
  { path: 'resultat-quiz', loadChildren: './tab-quizz/resultat-quiz/resultat-quiz.module#ResultatQuizPageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
