import { Component } from '@angular/core';
import {  Router } from '@angular/router';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-tab-consulter',
  templateUrl: 'tab-consulter.page.html',
  styleUrls: ['tab-consulter.page.scss']
})
export class TabConsulterPage {

  constructor(private router: Router, public alertController: AlertController) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Work In Progress',
      message: 'Cette option n\'est pas encore disponible.',
      buttons: ['OK']
    });

    await alert.present();
  }
}
