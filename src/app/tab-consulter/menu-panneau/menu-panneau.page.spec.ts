import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPanneauPage } from './menu-panneau.page';

describe('MenuPanneauPage', () => {
  let component: MenuPanneauPage;
  let fixture: ComponentFixture<MenuPanneauPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPanneauPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPanneauPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
