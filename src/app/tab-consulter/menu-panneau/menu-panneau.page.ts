import { Component, OnInit } from '@angular/core';
import {Categorie} from '../../model/categorie';
import {CategorieService} from '../../services/categorie.service';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-menu-panneau',
  templateUrl: './menu-panneau.page.html',
  styleUrls: ['./menu-panneau.page.scss'],
})
export class MenuPanneauPage implements OnInit {

  categories: Categorie[];

  constructor(private categorieService: CategorieService, protected router: Router) {
    this.categories = categorieService.getAll();
  }

  ngOnInit() {
  }

  public onCategorieClick(i): void {
    const navigationExtras: NavigationExtras = {
      queryParams: { filter: 'categorie', id: i }
    };
    this.router.navigate(['tabs/tab-consulter/liste-panneau'], navigationExtras);
  }

}
