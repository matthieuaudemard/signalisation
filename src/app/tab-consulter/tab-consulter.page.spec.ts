import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabConsulterPage } from './tab-consulter.page';

describe('TabConsulter', () => {
  let component: TabConsulterPage;
  let fixture: ComponentFixture<TabConsulterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabConsulterPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabConsulterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
