import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListePanneauPage } from './liste-panneau.page';

describe('ListePanneauPage', () => {
  let component: ListePanneauPage;
  let fixture: ComponentFixture<ListePanneauPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListePanneauPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListePanneauPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
