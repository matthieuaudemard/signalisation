import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {Panneau} from '../../model/panneau';
import {PanneauService} from '../../services/panneau.service';
import {ModalController} from '@ionic/angular';
import {ThemeService} from '../../services/theme.service';

@Component({
    selector: 'app-liste-panneau',
    templateUrl: './liste-panneau.page.html',
    styleUrls: ['./liste-panneau.page.scss'],
})
export class ListePanneauPage implements OnInit {

    panneaux: Panneau[];
    filter: string;
    id: number;
    searchInputText = '';

    constructor(
        private modalController: ModalController,
        private panneauService: PanneauService,
        private route: ActivatedRoute,
        private router: Router,
        private themeService: ThemeService
    ) {
        this.route.queryParams.subscribe(params => {
            if (params) {
                this.filter = params.filter;
                this.id = +params.id;
            }
            this.initialize();
        });
    }

    ngOnInit() { }

    public initialize(): void {
        switch (this.filter) {
            case 'categorie':
                this.panneaux = this.panneauService.getAllByCategorie(this.id);
                break;
            case 'couleur':
                break;
            default:
                this.panneaux = this.panneauService.getAll();
        }
        if (this.searchInputText != null && this.searchInputText.length >= 3) {
            this.panneaux = this.filterByPattern(this.searchInputText);
        }
    }

    public onPanneauClick(item): void {
        const navigationExtras: NavigationExtras = {
            state: {
                panneau: item
            }
        };
        this.router.navigate(['tabs/tab-consulter/detail-panneau'], navigationExtras);
    }

    public onInput(event: any) {
        this.initialize();
        this.searchInputText = event.target.value;
        if (this.searchInputText.trim().length >= 3) {
            this.panneaux = this.filterByPattern(this.searchInputText);
        }
    }

    private filterByPattern(pattern: string) {
        pattern = pattern.trim().toLowerCase();
        return  this.panneaux.filter(panneau => panneau.titre.toLowerCase().indexOf(pattern) > -1);
    }

    public onClear(event: any) {
        this.searchInputText = '';
        this.initialize();
    }
}
