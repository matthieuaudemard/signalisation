import {Component, OnInit} from '@angular/core';
import {Categorie} from '../../model/categorie';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-detail-categorie',
  templateUrl: './detail-categorie.page.html',
  styleUrls: ['./detail-categorie.page.scss'],
})
export class DetailCategoriePage implements OnInit {

  private categorie: Categorie;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.categorie = this.router.getCurrentNavigation().extras.state.categorie;
      }
    });
  }

  ngOnInit() {
  }

}
