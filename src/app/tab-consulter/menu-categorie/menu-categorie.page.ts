import {Component, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {Categorie} from '../../model/categorie';
import {CategorieService} from '../../services/categorie.service';

@Component({
    selector: 'app-menu-categorie',
    templateUrl: './menu-categorie.page.html',
    styleUrls: ['./menu-categorie.page.scss'],
})
export class MenuCategoriePage implements OnInit {

    categories: Categorie[];

    constructor(
        private categorieService: CategorieService,
        protected router: Router) {
        this.categories = categorieService.getAll();
    }

    ngOnInit() {
    }

    onCategorieClick(item): void {
      const navigationExtras: NavigationExtras = {
        state: {
          categorie: item
        }
      };
      this.router.navigate(['tabs/tab-consulter/detail-categorie'], navigationExtras);
    }
}
