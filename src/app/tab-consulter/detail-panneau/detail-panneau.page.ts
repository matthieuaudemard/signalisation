import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Panneau} from '../../model/panneau';
import {Categorie} from '../../model/categorie';
import {CategorieService} from '../../services/categorie.service';

@Component({
  selector: 'app-detail-panneau',
  templateUrl: './detail-panneau.page.html',
  styleUrls: ['./detail-panneau.page.scss'],
})
export class DetailPanneauPage implements OnInit {

  private panneau: Panneau;
  private categorie: Categorie;

  constructor(private route: ActivatedRoute, private router: Router, private categorieService: CategorieService) {
    this.route.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.panneau = this.router.getCurrentNavigation().extras.state.panneau;
        this.categorie = categorieService.get(this.panneau.idCategorie);
        console.log(this.panneau.id);
      }
    });
  }

  ngOnInit() {
  }

}
