import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {ResultatQuizPage} from './resultat-quiz.page';

const routes: Routes = [
  {
    path: '',
    component: ResultatQuizPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResultatQuizPage]
})
export class ResultatQuizPageModule {}
