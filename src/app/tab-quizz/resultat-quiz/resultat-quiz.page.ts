import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-resultat-quiz',
    templateUrl: './resultat-quiz.page.html',
    styleUrls: ['./resultat-quiz.page.scss'],
})
export class ResultatQuizPage implements OnInit {

    score: number;
    oldScore: number;
    img: string;
    title: string;
    msg: string;

    constructor(private storage: Storage, private router: Router) {
        storage.get('oldScore')
            .then(oldScore => this.oldScore = oldScore);
        storage.get('score')
            .then(score => {
                this.score = score;
                if (score >= 7) {
                    this.img = '/assets/icon/good.png';
                    this.title = 'Félicitation';
                    this.msg = '';
                } else if (score >= 5) {
                    this.img = '/assets/icon/medium.png';
                    this.title = 'C\'est bien';
                    this.msg = '';
                } else {
                    this.img = '/assets/icon/bad.png';
                    this.title = 'Bof';
                    this.msg = '';
                }
                storage.set('oldScore', score);
            }).catch(() => this.router.navigate(['tabs/tab-quizz/']));
    }

    ngOnInit() {
    }

}
