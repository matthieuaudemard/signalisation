import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabQuizzPage } from './tab-quizz.page';

describe('TabQuizz', () => {
  let component: TabQuizzPage;
  let fixture: ComponentFixture<TabQuizzPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabQuizzPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabQuizzPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
