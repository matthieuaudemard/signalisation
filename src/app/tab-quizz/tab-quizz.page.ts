import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab-quizz',
  templateUrl: 'tab-quizz.page.html',
  styleUrls: ['tab-quizz.page.scss']
})
export class TabQuizzPage {

  constructor(private router: Router) {
  }

}
