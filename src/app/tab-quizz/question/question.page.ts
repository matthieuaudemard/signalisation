import {Component, OnInit} from '@angular/core';
import {QuestionService} from '../../services/question.service';
import {Question} from '../../model/question';
import {PanneauService} from '../../services/panneau.service';
import {Panneau} from '../../model/panneau';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';

@Component({
    selector: 'app-question',
    templateUrl: './question.page.html',
    styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

    timer = new class {
        private seconde;
        private timer;
        private completed = false;
        private limit: number;

        constructor(public page: QuestionPage) {
        }

        start(limit: number, step: number) {
            this.completed = false;
            this.seconde = 0;
            this.limit = limit;
            this.timer = setInterval(() => {
                if (this.seconde < this.limit && !this.isCompleted()) {
                    this.seconde += step;
                } else if (this.isCompleted()) {
                    this.seconde = this.limit;
                    clearInterval(this.timer);
                    this.page.validerReponse();
                } else {
                    this.completed = true;
                }
            }, 100);
        }

        public getTime(): number {
            return this.seconde / this.limit;
        }

        public isCompleted(): boolean {
            return this.completed;
        }

        public complete() {
            this.seconde = this.limit;
        }
    }(this);

    question: Question;
    questions: Question[];
    currQuestionIndex = 0;
    panneau: Panneau;
    reponseUtilisateur: number = null;
    score: number;

    constructor(
        private questionService: QuestionService,
        private panneauService: PanneauService,
        private storage: Storage,
        private router: Router,
    ) {
        this.questions = questionService.getQuestionnaire(1);
        this.question = this.questions[this.currQuestionIndex];
        this.panneau = panneauService.get(this.question.idPanneau);
        this.timer.start(1000, 10);
        this.storage
            .set('score', 0)
            .then(() => this.score = 0);
    }

    ngOnInit() {
    }

    getListItemColor(i: number): string {
        const isTimerCompleted = this.timer.isCompleted();
        const isReponseCorrect = (i === this.question.idSolution);
        const isReponseChoisie = (i === this.reponseUtilisateur);
        if (isTimerCompleted && isReponseCorrect) {
            return 'success';
        } else if (isTimerCompleted && isReponseChoisie && !isReponseCorrect) {
            return 'danger';
        } else if (!isTimerCompleted && this.reponseUtilisateur === i) {
            return 'medium';

        } else {
            return 'undefined';
        }
    }

    getPageTitle(): string {
        if (this.currQuestionIndex > 0 && this.currQuestionIndex < this.questions.length - 1) {
            return (this.currQuestionIndex + 1).toString() + 'ème question';
        } else if (this.currQuestionIndex === 0) {
            return '1ère question';
        }
        return 'Dernière question';
    }

    onValiderClick(): void {
        if (this.reponseUtilisateur != null) {
            this.timer.complete();
        }
    }

    validerReponse(): any {
        // compter les points etc.
        if (this.reponseUtilisateur === this.questions[this.currQuestionIndex].idSolution) {
            this.score++;
        }
    }

    isValiderButton(): boolean {
        return !this.timer.isCompleted();
    }

    isNext(): boolean {
        return this.timer.isCompleted() && this.currQuestionIndex < this.questions.length - 1;
    }

    isResult(): boolean {
        return this.timer.isCompleted() && this.currQuestionIndex >= this.questions.length - 1;
    }

    isRadioDisabled(i: number) {
        return this.timer.isCompleted() && this.question.idSolution !== i && this.reponseUtilisateur !== i;
    }

    onNextClick(): void {
        if (this.currQuestionIndex < this.questions.length - 1) {
            this.currQuestionIndex++;
            this.question = this.questions[this.currQuestionIndex];
            this.panneau = this.panneauService.get(this.question.idPanneau);
            this.reponseUtilisateur = null;
            this.timer.start(1000, 10);
        }
    }

    onResultClick(): void {
        this.storage
            .set('score', this.score)
            .then(() => {
                this.router.navigate(['tabs/tab-quizz/resultat-quiz']);
            });
    }
}
