import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TabsPage} from './tabs.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'tab-consulter',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../tab-consulter/tab-consulter.module').then(m => m.TabConsulterModule)
                    },
                    {
                        path: 'liste-panneau',
                        loadChildren: () =>
                            import('../tab-consulter/liste-panneau/liste-panneau.module').then(m => m.ListePanneauPageModule)
                    },
                    {
                        path: 'menu-panneau',
                        loadChildren: () =>
                            import('../tab-consulter/menu-panneau/menu-panneau.module').then(m => m.MenuPanneauPageModule)
                    },
                    {
                        path: 'detail-panneau',
                        loadChildren: () =>
                            import('../tab-consulter/detail-panneau/detail-panneau.module').then(m => m.DetailPanneauPageModule)
                    },
                    {
                        path: 'detail-categorie',
                        loadChildren: () =>
                            import('../tab-consulter/detail-categorie/detail-categorie.module').then(m => m.DetailCategoriePageModule)
                    },
                    {
                        path: 'menu-categorie',
                        loadChildren: () =>
                            import('../tab-consulter/menu-categorie/menu-categorie.module')
                                .then(m => m.MenuCategoriePageModule)
                    },
                ]
            },
            {
                path: 'tab-quizz',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../tab-quizz/tab-quizz.module').then(m => m.TabQuizzPageModule)
                    },
                    {
                        path: 'question',
                        loadChildren: () =>
                            import('../tab-quizz/question/question.module').then(m => m.QuestionPageModule)
                    },
                    {
                        path: 'resultat-quiz',
                        loadChildren: () =>
                            import('../tab-quizz/resultat-quiz/resultat-quiz.module').then(m => m.ResultatQuizPageModule)
                    },
                ]
            },
            {
                path: 'tab-preference',
                children: [
                    {
                        path: '',
                        loadChildren: () =>
                            import('../tab-preference/tab-preference.module').then(m => m.TabPreferencePageModule)
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/tab-consulter',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/tab-consulter',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
