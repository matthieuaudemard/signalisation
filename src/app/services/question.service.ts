import {Injectable} from '@angular/core';
import {Question} from '../model/question';

@Injectable({
    providedIn: 'root'
})
export class QuestionService {

    questions: Question[] = [
      {
        id: 1,
        idCategorie: 3,
        idPanneau: 80,
        libelle: 'Que signifie ce panneau ?',
        reponses: [
          'Qu\'une aire de repos est à votre disposition au péage',
          'Qu\'il est interdit de s\'arrêter au péage',
          'Qu\'il faut s\'arrêter au poste de péage'
        ],
        idSolution: 2
      },
        {
            id: 2,
            idCategorie: 3,
            idPanneau: 56,
            libelle: 'L\'accès interdit s\'applique aux véhicules :',
            reponses: [
                'Transportant des produits explosifs ou facilement inflammables',
                'Transportant des matières dangereuses',
                'Equipés de gyrophares'
            ],
            idSolution: 0
        },
        {
            id: 3,
            idCategorie: 1,
            idPanneau: 20,
            libelle: 'Les usagers de la route doivent prendre garde :',
            reponses: [
                'aux marrées',
                'aux déformations de la chaussée',
                'aux cassis ou dos d\'âne'
            ],
            idSolution: 2
        },
        {
            id: 4,
            idCategorie: 1,
            idPanneau: 12,
            libelle: 'Ce panneau indique :',
            reponses: [
                'un virage à droite ou à gauche',
                'un danger à droite',
                'un virage à droite'
            ],
            idSolution: 2
        },
        {
            id: 5,
            idCategorie: 1,
            idPanneau: 27,
            libelle: 'Ce panneau alerte d\'un danger en provenance :',
            reponses: [
                'des pâturages environnants',
                'd\'un passage à niveau muni de barrières',
                'd\'un passage à niveau sans barrière'
            ],
            idSolution: 1
        },
        {
            id: 6,
            idCategorie: 1,
            idPanneau: 21,
            libelle: 'Ce panneau désigne :',
            reponses: [
                'un ralentisseur de type dos d\'âne',
                'la présence de trous dans la chaussée',
            ],
            idSolution: 0
        },
        {
            id: 7,
            idCategorie: 3,
            idPanneau: 55,
            libelle: 'A quoi correspond l\'intervalle de 70m entre les 2 véhicules ?',
            reponses: [
                'à l\'intevalle maximum à respecter',
                'à l\'intevalle minimum à respecter',
            ],
            idSolution: 1
        },
        {
            id: 8,
            idCategorie: 3,
            idPanneau: 62,
            libelle: 'Ce panneau signifie qu\'il est interdit :' ,
            reponses: [
                'de faire demi-tour jusqu\'à la prochaine intersection incluse',
                'de dépasser par la gauche',
                'de se garer en marche arrière',
            ],
            idSolution: 0
        },
        {
            id: 9,
            idCategorie: 3,
            idPanneau: 84,
            libelle: 'Que signifie ce panneau ?' ,
            reponses: [
                'une interdiction de stationnement',
                'une interdiction d\'arrêt et de stationnement',
                'une interdiction d\'arrêt et de stationnement les jours pairs',
            ],
            idSolution: 1
        },
        {
            id: 10,
            idCategorie: 3,
            idPanneau: 39,
            libelle: 'Que signifie ce panneau ?' ,
            reponses: [
                'Circulation interdite à tous les véhicules dans les deux sens',
                'fin d\'interdiction',
                'aucune interdiction sur cette zone',
            ],
            idSolution: 0
        },
        {
            id: 11,
            idCategorie: 3,
            idPanneau: 82,
            libelle: 'Le stationnement est :' ,
            reponses: [
                'autorisé uniquement du 1er au 15 du mois',
                'interdit uniquement du 1er au 15 du mois',
            ],
            idSolution: 1
        },
        {
            id: 12,
            idCategorie: 3,
            idPanneau: 85,
            libelle: 'Ce panneau d\'interdiction s\'adresse :' ,
            reponses: [
                'aux véhicules à moteurs à l\'exception des cyclomoteurs',
                'aux véhicules à moteur',
            ],
            idSolution: 0
        },
        {
            id: 13,
            idCategorie: 3,
            idPanneau: 42,
            libelle: 'Ce panneau d\'interdiction s\'adresse :' ,
            reponses: [
                'aux véhicules dont la largeur est supérieure à 2,5m',
                'à tous les véhicules dans 2,5m',
                'aux véhicules dont la longueur est supérieure à 2,5m',
            ],
            idSolution: 0
        },
        {
            id: 14,
            idCategorie: 3,
            idPanneau: 85,
            libelle: 'Ce panneau d\'interdiction s\'adresse :' ,
            reponses: [
                'aux véhicules à moteur à l\'exception des cyclomoteurs',
                'aux véhicules à moteur',
            ],
            idSolution: 0
        },
        {
            id: 15,
            idCategorie: 4,
            idPanneau: 114,
            libelle: 'Ce panneau indique :' ,
            reponses: [
                'une recommandation d\'allumer ses feux',
                'une obligation d\'allumer ses feux',
                'interdiction d\'allumer ses feux',
            ],
            idSolution: 1
        },
    ];

    constructor() {
    }

    public getAll(): Question[] {
        return this.questions;
    }

    public getQuestion(id: number): Question {
        return this.questions.find(q => q.id === id);
    }

    /**
     * Renvoi une série de 10 questions pour le questionnaire
     */
    public getQuestionnaire(id: number) {
        return this.shuffle(this.questions).slice(0, 10);
    }

  public shuffle(array): any[] {
    let currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
}
