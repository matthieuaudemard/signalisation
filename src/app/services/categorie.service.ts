import {Injectable} from '@angular/core';
import {Categorie} from '../model/categorie';

@Injectable({
    providedIn: 'root'
})
export class CategorieService {

    categories: Categorie[] = [
        {
            id: 1,
            titre: 'danger',
            ico: '/assets/categorie/ico/danger-00.png',
            img: '/assets/categorie/img/danger.JPG',
            detail: 'La signalisation routière de danger est l\'ensemble des équipements de voirie destinés à avertir les usagers de la route de l’existence et de la nature d’un danger.L\'objet de la signalisation de danger est d\'attirer de façon toute spéciale l\'attention des usagers de la route aux endroits où leur vigilance doit redoubler en raison de la présence d\'obstacles ou de points dangereux liés' +
                '' +
                'soit à la structure même de la route&nbsp;: virages, cassis ou dos-d’âne, chaussée rétrécie' +
                'soit à l\'état de la route ou son environnement chaussée glissante, chute de pierres' +
                'soit à des dispositions adaptées à la rencontre d\'autres voies de communication&nbsp;: pont mobile, barrière de passage à niveau' +
                'soit aux conditions de circulation des véhicules et des piétons&nbsp;: endroit fréquenté par les enfants, circulation à double sens succédant à une section à sens unique' +
                'soit à des dispositions ou des circonstances locales sortie d\'usine, voisinage d\'une carrière exploitée à la mine.'
        },
        {
            id: 2,
            titre: 'intersection - priorite',
            ico: '/assets/categorie/ico/intersection-00.png',
            img: '/assets/categorie/img/intersection.jpg',
            detail: 'Les panneaux de signalisation routière de priorité sont destinés à notifier ou à porter à la connaissance des usagers de la route des règles particulières de priorité à des intersections.' +
                'La signalisation de priorité est réalisée à l\'aide de panneaux de type AB au sens de l\'instruction interministérielle sur la signalisation routière, et correspond aux panneaux de type B au sens de la convention sur la signalisation routière signée à Vienne.' +
                ''
        },
        {
            id: 3,
            titre: 'interdiction',
            ico: '/assets/categorie/ico/interdiction-00.png',
            img: '/assets/categorie/img/prescription.jpg',
            detail: 'La signalisation routière d\'interdiction désigne l\'ensemble des équipements de signalisation (panneaux, balises) qui ont pour objet de notifier aux usagers de la route les interdictions spéciales prescrites par la réglementation locale.Les panneaux d\'interdiction sont à fond blanc, couronne et barre d\'interdiction rouges, symboles, lettres ou chiffres noirs à l\'exception' +
                '' +
                'du panneau B1 qui est à fond rouge et comporte une barre horizontale blanche' +
                'des panneaux B3, B3a, B15, B18b qui comportent dans le symbole une partie rouge' +
                'du panneau B18a dont le symbole comporte une explosion de couleur rouge et orange' +
                'du panneau B18c où le carré est de couleur orange' +
                'des panneaux B6a et B6d qui sont à fond bleu. Les inscriptions des panneaux B6a2 et B6a3 sont de couleur blanche' +
                'des panneaux B6b qui sont constitués par la représentation d\'un panneau B6a, sur un carré à fond blanc bordé d\'un listel rouge. Les autres symboles ou inscriptions éventuelles sont noirs.' +
                'des panneaux B30 qui sont constitués par la représentation d\'un panneau B14 dans un rectangle à fond blanc bordé d\'un listel rouge. Les autres symboles et inscriptions sont noirs.' +
                'Les couronnes des panneaux d\'interdiction et le panneau B1 ne doivent porter aucune inscription.' +
                ''
        },
        {
            id: 4,
            titre: 'obligation',
            detail: 'La signalisation routière d’obligation est l\'ensemble des équipements de la voirie destinés à enjoindre aux usagers de la route le respect de certaines prescriptions impératives locales.',
            ico: '/assets/categorie/ico/obligation-00.png',
            img: null
        },
        {
            id: 5,
            titre: 'indication',
            ico: '/assets/categorie/ico/indication-00.png',
            img: null,
            detail: 'L\'objet de la signalisation d\'indication est de porter à la connaissance des usagers de la route des informations utiles à la conduite des véhicules : indications relatives à l\'usage et à la praticabilité des voies, annonce de certains aménagements. Certains de ces signaux recouvrent des prescriptions particulièresA 1.' +
                '' +
                'La signalisation d\'indication est réalisée à l\'aide de panneaux de type C au sens de l\'instruction interministérielle sur la signalisation routièreA 2, et correspond aux panneaux de type E au sens de la convention de Vienne sur la signalisation routière. '
        },
        {
            id: 6,
            titre: 'services',
            ico: '/assets/categorie/ico/insolite-00.png',
            img: null,
            detail: 'L\'objet de la signalisation des services est de porter à la connaissance des usagers de la route la proximité ou la présence de services ou d\'installations rares ou isolés, susceptibles de leur être utiles et accompagnés, selon la nature de ces services ou installations, d\'une possibilité de stationnement.' +
                'La signalisation de services est réalisée à l\'aide de panneaux de type CE et/ou du panneau C1a.' +
                'Elle peut être complétée par des panneaux de type D.' +
                'Les panneaux de type CE, bien que signalant des services, ne doivent pas comporter de raison sociale à l\'exception du panneau CE15 dans certaines conditions.' +
                ''
        },
    ];

    constructor() {
    }

    public getAll() {
        return this.categories;
    }

    public get(id: number): Categorie {
        return this.categories.find(categorie => categorie.id === id);
    }

}
