import {Injectable, Renderer2, RendererFactory2, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Storage} from '@ionic/storage';
import {Theme} from '../model/theme';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    renderer: Renderer2;

    themes: Theme[] =  [
        {name: 'dark-theme', label: 'theme sombre'},
        {name: 'light-theme', label: 'theme lumineux'},
    ];

    currentTheme = 'light-theme';

    constructor(
        private rendererFactory: RendererFactory2,
        @Inject(DOCUMENT) private document: Document,
        private storage: Storage
    ) {
        this.renderer = this.rendererFactory.createRenderer(null, null);
    }

    public applyCurrentTheme() {
        this.storage.get('theme')
            .then(theme => this.applyTheme(theme))
            .catch(() => this.applyTheme('light-theme'));
    }

    public applyTheme(theme: string): void {
        if (this.themes.find(t => t.name === theme)) {
            this.storage
                .set('theme', theme)
                .then(currentTheme => {
                    this.renderer.removeClass(this.document.body, this.currentTheme);
                    this.renderer.addClass(this.document.body, currentTheme);
                    this.currentTheme = currentTheme;
                });
        }
    }

    public isDarkTheme(): boolean {
        return this.currentTheme === 'dark-theme';
    }

}
