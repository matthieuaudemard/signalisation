import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabPreferencePage } from './tab-preference.page';

describe('TabPreferencePage', () => {
  let component: TabPreferencePage;
  let fixture: ComponentFixture<TabPreferencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabPreferencePage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabPreferencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
