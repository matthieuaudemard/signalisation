import {Component} from '@angular/core';
import {ThemeService} from '../services/theme.service';

@Component({
    selector: 'app-tab-preference',
    templateUrl: 'tab-preference.page.html',
    styleUrls: ['tab-preference.page.scss']
})
export class TabPreferencePage {

    darkTheme: boolean;

    constructor(private themeService: ThemeService) {
        this.darkTheme = themeService.isDarkTheme();
    }

    public toggleTheme() {
        this.themeService.applyTheme(this.darkTheme ? 'dark-theme' : 'light-theme');
    }
}
