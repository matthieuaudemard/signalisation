export class Categorie {
    id: number;
    titre: string;
    detail: string;
    ico: string;
    img: string;
}
