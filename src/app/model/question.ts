export class Question {
    id: number;
    idCategorie: number;
    idPanneau: number;
    libelle: string;
    reponses: string[];
    idSolution: number;
}
