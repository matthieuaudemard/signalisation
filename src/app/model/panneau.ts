export class Panneau {
    id: number;
    idCategorie: number;
    img: string;
    commentaire: string;
    couleur: string;
    titre: string;
}
